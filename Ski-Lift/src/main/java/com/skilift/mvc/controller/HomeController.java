package com.skilift.mvc.controller;

import com.skilift.mvc.model.*;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.Date;

@Controller
public class HomeController implements ResourceLoaderAware {

    private BookingManager mgr;

    public HomeController(){
        mgr = BookingManager.get();
    }

    @RequestMapping(value="/", method = RequestMethod.GET)
    public ModelAndView showPreviouse(ModelMap model)
    {
        return new ModelAndView("login", "command", new LoginForm());
    }

    @RequestMapping(value="/login", method = RequestMethod.GET)
    public ModelAndView showLogin(ModelMap model)
    {
        return new ModelAndView("login", "command", new LoginForm());
    }

    @RequestMapping(value="/register", method = RequestMethod.GET)
    public ModelAndView showRegister(ModelMap model)
    {
        return new ModelAndView("register", "command", new LoginForm());
    }

    @RequestMapping(value="/home", method = RequestMethod.GET)
    public String showHome(ModelMap model)
    {
        return "home"; // loads home.jsp
    }

    @RequestMapping(value="/rent", method = RequestMethod.GET)
    public String showRent(ModelMap model)
    {
        return "rent"; // loads home.jsp
    }

    @RequestMapping(value="/confirmation", method = RequestMethod.GET)
    public String showConfirmation(ModelMap model, @RequestParam String lift, @RequestParam String seats) throws Exception {
        int idVal = Integer.parseInt(lift);
        int seatsVal = Integer.parseInt(seats);
        Date d = new Date();
        int code = mgr.book(idVal, d, 1, 1, seatsVal, new DebitCard(200));

        model.addAttribute("code", code);
        model.addAttribute("seats", seatsVal);

        return "confirmation";
    }

    @RequestMapping(value="/rentLift", method = RequestMethod.GET)
    public ModelAndView rentLift(ModelMap model, @RequestParam String lift, @RequestParam String seats) throws Exception
    {
        try {
            int idVal = Integer.parseInt(lift);
            int seatsVal = Integer.parseInt(seats);
            Date d = new Date();
            int code = mgr.book(idVal, d, 1, 1, seatsVal, new DebitCard(200));

            ModelAndView modelAndView = new ModelAndView("confirmation");

            //Add the userId to the request so it can be displayed in userinfo page
            modelAndView.addObject("code", code);
            modelAndView.addObject("seats", seatsVal);
            return modelAndView;
        }
        catch (NotLoggedOnException ex)
        {
            return new ModelAndView("login");
        }
    }

    @RequestMapping(value="/getNearbyLifts", method = RequestMethod.GET)
    public @ResponseBody String sendLiftsData(ModelMap model, @RequestParam String postalCode)
    {
        String data= "";
        try {
            for( Lift lift : mgr.getLifts(Integer.parseInt(postalCode)))
            {
                data += lift.getId() + "|" + lift.getName() + "|" + lift.getCapacity() + "|"+ lift.getType() + "\n";
            }
        }
        catch (NotLoggedOnException ex)
        {
            data = "InvalidLoggin";
        }


        return data;
    }

    @RequestMapping(value="/getLifts", method = RequestMethod.GET)
    public @ResponseBody String sendLiftsData(ModelMap model)
    {
        String data= "";

        try
        {
            data= "";
            for( Lift lift : mgr.getLifts())
            {
                data += lift.getId() + "|" + lift.getName() + "|" + lift.getCapacity() + "|"+ lift.getType() + "\n";
            }
        }
        catch (NotLoggedOnException ex)
        {
            data = "InvalidLoggin";
        }

        return data;
    }

    @RequestMapping(value="/getBookings", method = RequestMethod.GET)
    public @ResponseBody String sendBookingsData(ModelMap model)
    {
        String data= "";
        try
        {
            data= "";
            for(Booking booking : mgr.getBookings())
            {
                data += booking.getId() + "|" + booking.getLift().getName() + "|" + booking.getFrom() + "|"+ booking.getTo() + "\n";
            }
        }
        catch (NotLoggedOnException ex)
        {
            data = "InvalidLoggin";
        }

            return data;
        }

    @RequestMapping(value="/liftInfo", method = RequestMethod.GET)
    public @ResponseBody String sendLiftData(ModelMap model, @RequestParam String id)
    {
        String data  = "";
        Lift lift = mgr.getLift(Integer.parseInt(id));
        if(lift != null) {
            data = lift.getId() + "|" + lift.getName() + "|" + lift.getCapacity() + "|" + lift.getType();
        }

        return data;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {

        Resource res = resourceLoader.getResource("/data/ski-lift.sqlite3");

        try
        {
            mgr.init(res.getFile().getPath());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}