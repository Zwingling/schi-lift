package com.skilift.mvc.controller;

import com.skilift.mvc.model.BookingManager;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller

public class LoginController implements ResourceLoaderAware
{
    private ResourceLoader resourceLoader;

    @RequestMapping(value = "/authentification", method = RequestMethod.POST)
    public ModelAndView showLogin(@ModelAttribute("login") LoginForm login, BindingResult result)
    {
        String email = login.getEmail();
        String pswd = login.getPassword();

        if(!BookingManager.get().logon(email, pswd)) {
            ModelAndView view = new ModelAndView("login");
            view.addObject("errorMsg", "Invalid Login");
            return view;
        }

        return new ModelAndView("home");
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String showRegister(@ModelAttribute("register") LoginForm login, BindingResult result)
    {
        String email = login.getEmail();
        String pswd = login.getPassword();

        if(!BookingManager.get().addUser(email, pswd)) {
            // TODO display error
            throw new Error("could not register");
        }

        return "home";
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }
}