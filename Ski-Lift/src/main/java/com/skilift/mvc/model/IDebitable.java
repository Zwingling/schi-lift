package com.skilift.mvc.model;

/**
 * Created by Florian on 18.01.15.
 */
public interface IDebitable {
    public boolean debit(double amount);
}
