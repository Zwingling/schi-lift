package com.skilift.mvc.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Florian on 18.01.15.
 */

@DatabaseTable(tableName = "lifts")
public class Lift {

    @DatabaseField(columnName = "id", id = true)
    private int id;

    @DatabaseField(columnName = "name", width = 64)
    private String name;

    @DatabaseField(columnName = "corporation", width = 64)
    private String corporation;

    @DatabaseField(columnName = "postal_code")
    private int postalCode;

    @DatabaseField(columnName = "type", width = 64)
    private String type;

    @DatabaseField(columnName = "capacity")
    private int capacity;

    public Lift() {

    }

    public Lift(int id, String name, String corporation, int postalCode, String type, int capacity) {
        this.id = id;
        this.name = name;
        this.corporation = corporation;
        this.postalCode = postalCode;
        this.type = type;
        this.capacity = capacity;
    }

    private static final int NameIdx = 0;
    private static final int IdIdx = 1;
    private static final int PostalCodeIdx = 2;

    public int getCapacity() {
        return capacity;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCorporation() {
        return corporation;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public String getType() {
        return type;
    }
}
