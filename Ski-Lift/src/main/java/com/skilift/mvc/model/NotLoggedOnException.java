package com.skilift.mvc.model;

/**
 * Created by Florian on 18.01.15.
 */
public class NotLoggedOnException extends RuntimeException
{
    public NotLoggedOnException() {
        super("No user logged on");
    }
}
