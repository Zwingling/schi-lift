package com.skilift.mvc.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Florian on 25.01.15.
 */

@DatabaseTable(tableName = "users")
public class User {

    @DatabaseField(columnName = "name", id = true, width = 64)
    private String userName;

    public String getPassword() {
        return password;
    }

    @DatabaseField(columnName = "password", width = 64)
    private String password;

    public User()
    {

    }

    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }
}
