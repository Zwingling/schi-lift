package com.skilift.mvc.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by Florian on 18.01.15.
 */
@DatabaseTable(tableName = "bookings")
public class Booking {

    @DatabaseField(columnName = "id", generatedId = true)
    private int id;

    @DatabaseField(columnName = "lift_id", foreign = true)
    private Lift lift;

    @DatabaseField(columnName = "user", foreign = true)
    private User user;

    @DatabaseField(columnName = "from", dataType = DataType.DATE_STRING, format = "yyyy-MM-dd HH:mm:ss")
    private Date from;

    @DatabaseField(columnName = "to", dataType = DataType.DATE_STRING, format = "yyyy-MM-dd HH:mm:ss")
    private Date to;

    @DatabaseField(columnName = "uses")
    private int uses;

    public Booking() {

    }

    public Booking(Date from, Date to, int uses, Lift lift, User user) {
        this.from = from;
        this.to = to;
        this.uses = uses;
        this.lift = lift;
        this.user = user;
    }

    public boolean use() {
        Date date = new Date();
        if(date.after(from) && date.before(to) && uses > 0)
        {
            --uses;
            return true;
        }

        return false;
    }

    public int getId() {
        return id;
    }

    public int getUses() {
        return uses;
    }

    public Lift getLift() {
        return lift;
    }

    public User getUser() {
        return user;
    }

    public Date getFrom() {
        return from;
    }

    public Date getTo() {
        return to;
    }
}
