package com.skilift.mvc.model;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;


public class BookingManager
{
    private Dao<Lift, Integer> lifts;
    private Dao<Booking, Integer> bookings;
    private Dao<User, String> users;
    private User currentUser;

    public BookingManager() {

    }

    public void init(String dataUrl) {

        try
        {
            dataUrl = dataUrl.replace('\\', '/');

            Class.forName("org.sqlite.JDBC");
            String path = "jdbc:sqlite:" + dataUrl;
            ConnectionSource connectionSource = new JdbcConnectionSource(path);

            lifts = DaoManager.createDao(connectionSource, Lift.class);
            bookings = DaoManager.createDao(connectionSource, Booking.class);
            users = DaoManager.createDao(connectionSource, User.class);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Collection<Lift> getLifts() throws NotLoggedOnException {

        ensureLoggedOn();

        try {
            return lifts.queryForAll();
        } catch (SQLException e) {
            return null;
        }
    }

    public Collection<Lift> getLifts(int postalCode)  throws NotLoggedOnException {
        ensureLoggedOn();

        try {
            return lifts.queryForEq("postal_code", postalCode);
        } catch (SQLException e) {
            return null;
        }
    }

    public Lift getLift(int id) {
        try {
            return lifts.queryForId(id);
        } catch (SQLException e) {
            return null;
        }
    }

    public Collection<Booking> getBookings() throws NotLoggedOnException {

        ensureLoggedOn();

        try {
            return bookings.queryForEq("user", currentUser.getUserName());
        } catch (SQLException e) {
            return null;
        }
    }

    public Booking getBooking(int id) throws InsufficientPermissionsException {

        ensureSuperUserLoggedOn();

        try {
            return bookings.queryForId(id);
        } catch (SQLException e) {
            return null;
        }
    }

    public void logoff() {
        currentUser = null;
    }

    public boolean logon(String username, String password) {

        logoff();

        try {
            User user = users.queryForId(username);
            if (user != null && user.getPassword().equals(password)) {
                currentUser = user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return currentUser != null;
    }

    public boolean loggedOn(){
        return currentUser != null;
    }

    private void ensureLoggedOn() throws NotLoggedOnException {
        if(!loggedOn()) throw new NotLoggedOnException();
    }

    private void ensureSuperUserLoggedOn() throws InsufficientPermissionsException {
        if(!loggedOn() || !currentUser.getUserName().equals("admin")){
            throw new InsufficientPermissionsException();
        }
    }

    public boolean addUser(String username, String password) {
        try {
            users.create(new User(username, password));
        } catch (SQLException e) {
            return false;
        }

        return true;
    }

    private static final double pricePerUse = 1.0;
    private static final double pricePerDay = 0.5;
    public double getPrice(int duration, int uses, int seats) {

        if(uses < 1 || seats < 1 || duration < 0 || duration > 7) {
            throw new IllegalArgumentException();
        }

        return (uses * pricePerUse + duration * pricePerDay) * seats;
    }

    public int book(int liftId, Date from, int duration, int uses, int seats, IDebitable paymentMethod) throws Exception {

        ensureLoggedOn();

        Lift lift;
        if(paymentMethod == null ||
           uses < 1 ||
           seats < 1 ||
           duration < 0 ||
           duration > 7 ||
           (lift = lifts.queryForId(liftId)) == null ||
           lift.getCapacity() < seats)
        {
            throw new IllegalArgumentException();
        }

        Calendar.getInstance().add(Calendar.DAY_OF_YEAR, duration);
        Date to = Calendar.getInstance().getTime();

        if(paymentMethod.debit(getPrice(duration, uses, seats)))
        {
            Booking booking = new Booking(from, to, uses, lifts.queryForId(liftId), currentUser);
            bookings.create(booking);
            return booking.getId();
        }
        else
        {
            throw new Exception("insufficient credits");
        }
    }

    public boolean redeem(Booking booking) {

        if(booking != null){
            if(!booking.use())
            {
                try {
                    bookings.delete(booking);
                } catch (SQLException e) {

                }
                return false;
            }

            if(booking.getUses() == 0)
            {
                try {
                    bookings.delete(booking);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            return true;
        }

        return false;
    }

    private static BookingManager mgr = null;
    public static BookingManager get() {
        if(mgr == null) mgr = new BookingManager();
        return mgr;
    }
}
