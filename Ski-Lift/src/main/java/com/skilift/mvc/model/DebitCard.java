package com.skilift.mvc.model;

/**
 * Created by Florian on 18.01.15.
 */
public class DebitCard implements IDebitable {

    private double balance;

    public DebitCard(double balance) {
        if(balance < 0) {
            throw new IllegalArgumentException();
        }

        this.balance = balance;
    }

    @Override
    public boolean debit(double amount) {
        if(amount > balance) return false;
        else balance -= amount;
        return true;
    }
}
