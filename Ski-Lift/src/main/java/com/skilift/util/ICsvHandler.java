package com.skilift.util;

/**
 * Created by Florian on 18.01.15.
 */
public interface ICsvHandler
{
    public void OnNewLine(String[] values);
}
