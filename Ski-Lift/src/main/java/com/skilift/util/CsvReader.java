package com.skilift.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class CsvReader {
    public static void Read(File file, ICsvHandler handler) throws Exception {

        BufferedReader br = null;
        String line = "";
        final String cvsSplitBy = ";";

        try {
            br = new BufferedReader(new FileReader(file));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] values = line.split(cvsSplitBy);

                handler.OnNewLine(values);


            }
        }
        catch (Exception e) {
            throw e;
        }
    }
}
