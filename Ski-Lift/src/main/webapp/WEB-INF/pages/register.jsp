<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Login</title>

    <link href="../resources/css/style.css" rel="stylesheet"/>
    <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<h1 style="text-align: center; font-size: 40px; color: #00a1d2; margin-top: 15px; margin-bottom: -40px" >Sign Up</h1>
<form method="post" action="addUser" class="login">
    <p>
        <label for="email">Email:</label>
        <input type="text" name="email" id="email" path="email" value="${email}">
    </p>

    <p>
        <label for="password">Password:</label>
        <input type="password" name="password" id="password" path="password" value="${password}">
    </p>

    <p class="login-submit">
        <button type="submit" class="login-button">Register</button>
    </p>
</form>
</body>
</html>
