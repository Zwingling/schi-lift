<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Home</title>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <link href="../resources/css/rent.css" rel="stylesheet"/>
    <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<script>

    function selectLift()
    {
        window.location = "rent?lift="+this.id;
    }

    function getLifts(plz)
    {
        var strData = "<div class='header-row row'>" +
                "<span class='cell primary'>Name</span>" +
                "<span class='cell'>Seats</span>" +
                "</div>";

        $.get("getNearbyLifts?postalCodeStr=" + plz, function (data)
        {
            if (data == "InvalidLoggin") {
                window.location = "login";
            }

            var c = 0;
            var res = data.split("\n");
            res.forEach(function (lift) {
                if (lift == "")
                    return;
                var liftData = lift.split("|");
                var id = liftData[0];
                var name = liftData[1];
                var capacity = liftData[2];
                var type = liftData[3];

                strData += "<div class='row' id='" + id + "' onclick='selectLift.call(this)'>" +
                        "<input type='radio' name='expand'>" +
                        "<span class='cell primary' data-label='Vehicley'>" + name + "</span>" +
                        "<span class='cell' data-label='Exterior'>" + capacity + "</span>" +
                        "</div>";

                c++;
            });

            $('#table').html(strData);
            $('#tableTitle').text("Available lifts: " + c+ " found");
            return;
        });

        $('#tableTitle').text("Available lifts: 0 found");
        $('#table').html(strData);
    }

    $( document ).ready(function()
    {
        if (!navigator.geolocation)
            return;

        navigator.geolocation.getCurrentPosition(function(position) {
            var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + position.coords.latitude + "," + position.coords.longitude + "&sensor=true";

            $.ajax({
                url: url,
                success: function (data) {

                    var plz = "";
                    data.results.forEach(function (res) {
                        res.address_components.forEach(function (component) {
                            if (component.types[0] == "postal_code") {
                                plz = component.long_name;
                                return;
                            }
                        })
                        if (plz != "")
                            return;
                    })

                    if (plz != "") {

                        $('#iPlz').val(plz);
                        getLifts(plz);

                    }
                },
                error: function ()
                {
                    $.get( "getLifts", function( data )
                    {
                        var strData = "<div class='header-row row'>"+
                                "<span class='cell primary'>Name</span>"+
                                "<span class='cell'>Seats</span>"+
                                "</div>";
                        var res = data.split("\n");
                        res.forEach(function(lift)
                        {
                            if(lift=="")
                                return;

                            var liftData = lift.split("|");
                            var id = liftData[0];
                            var name = liftData[1];
                            var capacity = liftData[2];
                            var type = liftData[3];

                            strData += "<div class='row' id='"+id+"' onclick='selectLift.call(this)'>"+
                                    "<input type='radio' name='expand'>"+
                                    "<span class='cell primary' data-label='Vehicley'>"+name+"</span>"+
                                    "<span class='cell' data-label='Exterior'>"+capacity+"</span>"+
                                    "</div>"
                        });
                        $('#table').html(strData);
                    });
                },

                timeout: 2000 //in milliseconds
            });
        });
    });

    function onPlzChanged()
    {
        getLifts($('#iPlz').val());
    }

</script>

<div id="container">
    <p style="font-size: 20px; color: #00a1d2; margin-right: 18px;">PLZ<input id="iPlz" type="text" name="plz" size="10" style="position: absolute; margin-left: 10px" onchange="onPlzChanged()"></p>

    <div class="caption" id="tableTitle">Available lifts</div>
    <div id="table">
        <div class="header-row row">
            <span class="cell primary">Name</span>
            <span class="cell">Seats</span>
        </div>
    </div>
</div>
</body>
</html>
