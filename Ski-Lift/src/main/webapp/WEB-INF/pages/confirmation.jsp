<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Confirmation</title>

    <link href="../resources/css/rent.css" rel="stylesheet"/>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<script>

    function getQueryParams(qs) {
        qs = qs.split("+").join(" ");

        var params = {}, tokens,
                re = /[?&]?([^=]+)=([^&]*)/g;

        while (tokens = re.exec(qs)) {
            params[decodeURIComponent(tokens[1])]
                    = decodeURIComponent(tokens[2]);
        }

        return params;
    }


    $( document ).ready(function()
    {
        var seats = ${seats};

        var params = getQueryParams(document.location.search);

        if(seats != "1" )
            $('#seats').text(params.seats +" Seats reserved");
        else
            $('#seats').text("1 Seat reserved");
    });


</script>
<body>
    <div class="caption">Confirmation</div>
    <p id="code"  style="font-size: 40px; color: #FFFFFF;" >
        <b>Code: ${code}</b><br>
    </p>
    <p id="seats" style="font-size: 32px; color: #00a1d2;"></p>
</body>
</html>
