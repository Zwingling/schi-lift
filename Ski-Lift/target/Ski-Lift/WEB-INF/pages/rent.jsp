<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Rent</title>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>

    <link href="../resources/css/rent.css" rel="stylesheet"/>
    <!--[if lt IE 9]><script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>
<body>
<script>
    function getQueryParams(qs) {
        qs = qs.split("+").join(" ");

        var params = {}, tokens,
                re = /[?&]?([^=]+)=([^&]*)/g;

        while (tokens = re.exec(qs)) {
            params[decodeURIComponent(tokens[1])]
                    = decodeURIComponent(tokens[2]);
        }

        return params;
    }

    $( document ).ready(function()
    {
        var params = getQueryParams(document.location.search);
        $.get( "liftInfo?id="+params.lift, function( data )
        {
            if(data == "InvalidLoggin")
            {
                window.location = "login";
            }

            console.log(data);

            var liftData = data.split("|");
            var id = liftData[0];
            var name = liftData[1];
            var capacity = liftData[2];
            var type = liftData[3];

            console.log(type);
            if(type == "Schlepplift")
            {
                $('#lifttype').src = "../resources/img/schlepplift.jpg";
            }
            else if(type == "Sessellift ")
            {
                $('#lifttype').src = "../resources/img/sessellift.png";
            }
            else
            {
                $('#lifttype').attr('src', '../resources/img/gondel.jpg');
            }

            $('#liftName').text(name);
            $('#nseats').val(capacity);


            $("input").attr({
                "max" : capacity,        // substitute your own
                "min" : 1          // values (or variables) here
            });

            $( "#nseats" ).change(function() {
                if ($(this).val() > capacity)
                {
                    $(this).val(capacity);
                }
                else if ($(this).val() < 1)
                {
                    $(this).val(1);
                }
            });


        });
    });

    function onRent()
    {
        var params = getQueryParams(document.location.search);
        window.location = "rentLift?lift="+params.lift+"&seats="+$( "#nseats").val();
    }

</script>



<div id="container" style="text-align: center">
    <u style="color: #00a1d2;"><p id="liftName" style="color: #00a1d2; font-size: 40px; font-family: Helvetica;" >Lift Name</p></u>
    <p  style="font-size: 40px; color: #00a1d2;margin-left: -100px">Type:
    <image id="lifttype" src="../resources/img/lift2.png" width="56" height="56" style="position: absolute; margin-left: 30px"></image>
    </p>
    <form style="font-size: 40px; color: #00a1d2; margin-left: -100px">
        Seats:
        <input min="1" id="nseats" type="number" name="nSeats" style="position: absolute; width: 90px; height: 25px; margin-left: 20px; margin-top: 10px">
    </form>
    <input type="button" value="Rent" onclick="onRent()" style="color: #00a1d2; font-size: 40px; font-family: Helvetica; width:200px; margin-top: 50px"/>
</div>
</body>
</html>
